![alt strefakursow.pl](https://bedeprogramistka.pl/wp-content/uploads/2018/08/strefa-kursow-logo-1.jpg "Strefa Kursów Szkolenia Online")
# Meduza
**Opis:** Jest to repozytorium powiązane z kursem na stronie Strefa Kursów dostępna pod tym [adresem](https://example.strefakursow.pl)  

## Nasza Check lista do kursu

1. [x] Wprowadzenie  
   1. [x] Wstęp  
   2. [x] Jak korzystać z materiałów  
   3. [x] Krótkie omówienie projektów  
2. [x] Instalacja i konfiguracja GIT  
   1. [x] Konfiguracja środowiska Windows  
   2. [x] Konfiguracja i instalacja Visual Studio Code  
3. [x] Git Must Know  
   1. [x] Tworzymy pierwszy projekt w Gitlab i git clone  
   2. [x] Git commit - pierwsze zmiany w repo  
   3. [x] Markdown - Nasz plik README.md
   4. [x] .gitignore
   5. [x] Windows command line git
   6. [x] Gitlab branch
   7. [x] Merge Request
   8. [x] Staging i commiting w Web IDE
4. [x] Git pull, commit, push, tags, branch, merging and reverting
   1. [x] Git Pull i Commit
   2. [x] Używanie tagów
   3. [x] Używanie branches (branch)
   4. [x] Git Push Tags
   5. [x] Merging Branches
   6. [x] Rebasing
   7. [x] Reverting a commit - odwracanie zmian 

## Powiązane kursy

| NAZWA KURSU | ADRES URL | POZIOM KURSU |
| ----------- | --------- | ------------ |
| Dobre praktyki pracy w zespole | https://strefakursow.pl/kursy/programowanie/dobre_praktyki_pracy_w_zespole.html | Średniozaawansowany |
